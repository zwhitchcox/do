## Dependencies
[doctl](https://github.com/digitalocean/doctl)
[helm](https://helm.sh/)

## Running

1. To run, you need to add an ssh key to your account: https://cloud.digitalocean.com/account/security

2. Then you need to add an api token: https://cloud.digitalocean.com/account/api/tokens

3. Then export that api token to a variable called DOAT

4. Then clone the repo: git clone https://gitlab.com/zwhitchcox/do

5. Then run `./create-kluster.sh` -k={yourkey}, where `yourkey` is the *name* of the ssh key you added to you account. Not the key itself.

6. Then set kubectl to use your new kube config with `export KUBECONFIG=$PWD/kubeconfig` (this was copied from your new cluster)


//TODO
1. add prometheus
1. implement autoscaling kluster (hard)
