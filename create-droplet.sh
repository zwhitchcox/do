#!/bin/bash

for i in "$@"
do
  case $i in
    -k=*|--ssh-key-id=*)
      KEY_ID="${i#*=}"
      ;;
    *)
      ;;
  esac
  case $i in
    -n=*|--droplet-name=*)
      NAME="${i#*=}"
      ;;
    *)
      ;;
  esac
  case $i in
    -t=*|--tag=*)
      TAG="${i#*=}"
      ;;
    *)
      ;;
  esac
done

curl -sX POST -H "Content-Type: application/json" -H "Authorization: Bearer $DOAT" -d '{"name":"'"$NAME"'","region":"nyc3","size":"s-1vcpu-1gb","image":"ubuntu-18-04-x64","ssh_keys":["'"$KEY_ID"'"],"backups":false,"ipv6":true,"user_data":null,"private_networking":null,"volumes": null,"tags":["'$TAG'"]}' "https://api.digitalocean.com/v2/droplets" 

