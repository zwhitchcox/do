#!/bin/bash
# ssh root@ip "bash -s" < ./scripts/digitalocean/create-sudo-user-and-add-firewall.sh user password

USER=$1
PASSWORD=$2
sudo adduser \
  --system \
  --shell /bin/bash \
  --gecos `User for managing kubernetes` \
  --group $USER\
  --disabled-password \
  --home /home/$USER \
  $USER
echo -e "\nubuntu ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
rsync --archive --chown=$USER:$USER ~/.ssh /home/$USER
chown -R $USER:$USER /home/$USER
