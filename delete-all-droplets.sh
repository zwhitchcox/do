#!/bin/bash
for i in "$@"
do
  case $i in
    -t=*|--tag=*)
      TAG="${i#*=}"
      ;;
    *)
      ;;
  esac
done

#if [ -z $TAG ];
#then TAG="kube"
#fi

check_deleted() {
  ids=$(curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DOAT" "https://api.digitalocean.com/v2/droplets?page=1&per_page=10&tag_name=$TAG" | jq -r ".droplets[].id")
  len=$(echo $ids | wc -w)
  if [ "$len" -gt 0 ]; then
    for id in $ids
      do
        curl -X DELETE -H "Content-Type: application/json" -H "Authorization: Bearer $DOAT" "https://api.digitalocean.com/v2/droplets/$id" 
      done
    sleep 2
    check_deleted
  else
    echo "all droplets have been deleted!"
    exit
  fi

}

check_deleted
