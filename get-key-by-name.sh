#!/bin/bash
# Usage ./scripts/do/get-key-by-name.sh -n=yoga

for i in "$@"
do
  case $i in
    -n=*|--key-name=*)
      KEY_NAME="${i#*=}"
      ;;
    *)
      ;;
  esac
done


curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DOAT" "https://api.digitalocean.com/v2/account/keys" | jq -r 'reduce .ssh_keys[] as $item (""; if $item.name == "'"$KEY_NAME"'" then $item.id else . end)'
