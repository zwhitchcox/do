DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo creating droplet
doctl auth init
doctl compute tag create supergiant
doctl compute droplet create supergiant \
  --region nyc1 \
  --image ubuntu-18-04-x64 \
  --size s-1vcpu-1gb \
  --ssh-keys $FP \
  --tag-name supergiant

echo created droplet, waiting for ip...
ip=$(doctl compute droplet list --tag-name supergiant --format "PublicIPv4" | tail -1)
ssh-keyscan -H $ip >> ~/.ssh/known_hosts
echo 
echo new ip is $ip, now installing...
ssh root@"$ip" "bash -s" < "$DIR"/supergiant-install.sh
echo supergiant installed. server accessible at root@$ip
