curl https://github.com/supergiant/supergiant/releases/download/v1.1.0/supergiant-api-linux-amd64 -L -o /usr/bin/supergiantapi
curl https://github.com/supergiant/supergiant/releases/download/v1.1.0/supergiant-ui-linux-amd64 -L -o /usr/bin/supergiant
chmod +x /usr/bin/supergiant
chmod +x /usr/bin/supergiantapi
sudo curl https://gitlab.com/zwhitchcox/do/raw/master/config.json --create-dirs -o /etc/supergiant/config.json
sudo mkdir /var/lib/supergiant && sudo mkdir /var/log/supergiant
echo "[Unit]
Description=Supergiant Server
After=syslog.target
After=network.target
[Service]
ExecStart=/usr/bin/supergiant --config-file /etc/supergiant/config.json
Restart=on-abort
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/supergiant.service
#sudo supergiant --config-file /etc/supergiant/config.json
#sudo systemctl start supergiant.service
#sudo systemctl status supergiant.service
#sudo systemctl enable supergiant.service
